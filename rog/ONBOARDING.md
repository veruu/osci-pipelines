To onboard new components to the RHEL-on-GitLab pipeline:

1. Add new components to https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines/-/blob/main/rog/configs/ci-mediator/config.yaml
2. Update `rules` in `include` and `.exclude_rog_components` sections of https://gitlab.com/redhat/centos-stream/ci-cd/dist-git-gating-tests/-/blob/latest/global-tasks.yml
3. If you want to onboard the component to RoG for all of c8s, c9s and c10s branches, remove the component's entry in https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config/-/blob/master/resources/centos-distgits.yaml. Otherwise leave the file as is.
