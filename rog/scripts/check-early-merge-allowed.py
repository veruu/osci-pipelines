#!/usr/bin/python3
"""Check a list of files changed in a MR against two allowlists.
Print easily-parsable messages to indicate whether the changes in the
MR pass one or the other allowlists, or both."""

import os
import re
import sys

if __name__ == "__main__":

    if len(sys.argv) < 2:
        input_file = sys.stdin
    else:
        input_file = open(sys.argv[1], "r", encoding="utf-8")

    lines = input_file.readlines()

    early_merge_allowlist = [
        r"^\.gitignore$",
        r"^gating\.yaml$",
        r"^\.gitlab-ci\.yml$",
    ]
    skip_build_allowlist = [
        r"^rpminspect\.yaml$",
        r"^\.fmf/version$",
        r".+\.fmf$",
        r"^tests/.*",
    ]

    ALLOW_EARLY_MERGE = True
    ALLOW_SKIP_BUILD = True
    for line in lines:
        if not any(re.match(pattern, line) for pattern in early_merge_allowlist):
            ALLOW_EARLY_MERGE = False
            if not any(re.match(pattern, line) for pattern in skip_build_allowlist):
                ALLOW_SKIP_BUILD = False
        if not ALLOW_SKIP_BUILD and not ALLOW_EARLY_MERGE:
            break

    if ALLOW_EARLY_MERGE:
        print("ALLOW_EARLY_MERGE")
    if ALLOW_SKIP_BUILD:
        print("ALLOW_SKIP_BUILD")
