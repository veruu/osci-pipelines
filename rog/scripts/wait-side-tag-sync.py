#!/usr/bin/python3

import os
import sys
import time

import koji
import requests

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print(f"Usage: {os.path.basename(__file__)} SIDE_TAG DOWNSTREAM_SIDE_TAG")
        sys.exit(1)
    stream_sidetag = sys.argv[1]
    brew_sidetag = sys.argv[2]

    stream = koji.ClientSession("https://kojihub.stream.rdu2.redhat.com/kojihub")
    brew = koji.ClientSession("https://brewhub.engineering.redhat.com/brewhub")

    stream_sidetag_content = stream.listTagged(stream_sidetag, latest=True)
    stream_nvrs = {build["nvr"] for build in stream_sidetag_content}

    project_id = os.environ.get("CI_PROJECT_ID")
    mr_iid = os.environ.get("CI_MERGE_REQUEST_IID")
    gitlab_token = os.environ.get("GITLAB_TOKEN")
    comment_posted = False

    while True:
        brew_sidetag_content = brew.listTagged(brew_sidetag, latest=True)
        brew_complete = {
            build["nvr"]
            for build in brew_sidetag_content
            if build["state"] == koji.BUILD_STATES["COMPLETE"]
        }

        if brew_complete == stream_nvrs:
            print(f"Side-tags {stream_sidetag} and {brew_sidetag} are in sync.")
            if comment_posted:
                requests.post(
                    url=f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{mr_iid}/notes",
                    headers={"PRIVATE-TOKEN": gitlab_token},
                    data={
                        "body": f"<h4>All builds complete in {brew_sidetag}.</h4><br/>"
                        f"Side-tags {stream_sidetag} and {brew_sidetag} are now in sync."
                    },
                )

            sys.exit(0)

        print("\n")

        brew_nvrs = {build["nvr"] for build in brew_sidetag_content}

        brew_missing = ", ".join(sorted(list(stream_nvrs - brew_nvrs)))

        if brew_missing:
            print(
                f"Builds missing from downstream side-tag {brew_sidetag}: {brew_missing}"
            )

        # Prefer "\n".join() instead?
        brew_building = ", ".join(
            sorted(
                list(
                    {
                        build["nvr"]
                        for build in brew_sidetag_content
                        if build["state"] == koji.BUILD_STATES["BUILDING"]
                    }
                )
            )
        )

        if brew_building:
            print(f"Builds in BUILDING state: {brew_building}")

        brew_deleted = ", ".join(
            sorted(
                list(
                    {
                        build["nvr"]
                        for build in brew_sidetag_content
                        if build["state"] == koji.BUILD_STATES["DELETED"]
                    }
                )
            )
        )

        if brew_deleted:
            print(f"Builds in DELETED state: {brew_deleted}")

        brew_failed = ", ".join(
            sorted(
                list(
                    {
                        build["nvr"]
                        for build in brew_sidetag_content
                        if build["state"] == koji.BUILD_STATES["FAILED"]
                    }
                )
            )
        )

        if brew_failed:
            print(f"Builds in FAILED state: {brew_failed}")

        brew_canceled = ", ".join(
            sorted(
                list(
                    {
                        build["nvr"]
                        for build in brew_sidetag_content
                        if build["state"] == koji.BUILD_STATES["CANCELED"]
                    }
                )
            )
        )

        if brew_canceled:
            print(f"Builds in CANCELED state: {brew_canceled}")

        print(
            f"\nSide-tags {stream_sidetag} and {brew_sidetag} are not in sync.\n"
            f"Waiting until all builds in {brew_sidetag} are in COMPLETE state."
        )

        if not comment_posted:
            requests.post(
                url=f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{mr_iid}/notes",
                headers={"PRIVATE-TOKEN": gitlab_token},
                data={
                    "body": f"<h4>Side-tags {stream_sidetag} and {brew_sidetag} are not in sync.</h4><br/>"
                    "Waiting for Brew builds to finish. See build_rpm console for details."
                },
            )
            comment_posted = True

        time.sleep(60)
