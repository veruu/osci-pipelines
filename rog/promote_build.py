import argparse
import os
import koji


RHEL_URL = "https://brewhub.engineering.redhat.com/brewhub"
CS_URL = "https://kojihub.stream.rdu2.redhat.com/kojihub"

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--rhel-or-stream', choices=['rhel', 'centos-stream'])
    args = parser.parse_args()

    if args.rhel_or_stream == 'rhel':
        hub_url = RHEL_URL
    else:
        hub_url = CS_URL

    brew_session = koji.ClientSession(hub_url)

    CI_COMMIT_SHA = os.environ.get("CI_COMMIT_SHA")
    packageID = os.environ.get("CI_PROJECT_NAME")

    builds = brew_session.listBuilds(
        packageID=packageID,
        state=koji.BUILD_STATES["COMPLETE"],
        source=f"*#{CI_COMMIT_SHA}",
        draft=True,
    )
    build = sorted(builds, key=lambda d: d["completion_ts"], reverse=True)[0]
    print(f"Going to promote build {build['nvr']} completed at {build['completion_time']}.")
    brew_session.promoteBuild(build["build_id"])
