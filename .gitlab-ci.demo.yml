### Pipeline rules ###
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_EVENT_TYPE != "merge_train"'
      changes:
        - "*.{j2,py,xml,yaml,yml}"  # Don't trigger on changes to docs etc.
        - ".gitlab-ci.yml"

### Global variables ###
variables:
  TEST_BRANCH: "pipeline-test-$CI_MERGE_REQUEST_IID"
  TEST_PACKAGE_REPO: "myprojects/rpms/test-package"
  TEST_PACKAGE_DEFAULT_BRANCH: "main"
  PRODUCTION_PIPELINE_FILE: "production.yaml"

create_update_test_mr:
  ### Mirror Feature MR's lifecycle in Testing MR ###
  environment:
    name: pipeline-test-$CI_MERGE_REQUEST_IID
    on_stop: close_test_mr
  script:
    ### Set Testing MR attributes ###
    - MR_URL="$CI_MERGE_REQUEST_SOURCE_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_IID"
    - MR_TITLE="[AUTO] Test changes in $MR_URL"
    - |
      MR_DESCRIPTION="Changes to production pipeline are being requested in $CI_MERGE_REQUEST_SOURCE_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_IID. Please review pipeline triggered by this MR. Please do not merge."

    ### Obtain GitLab token as appropriate ###

    ### Clone Testing Project, switch to Testing MR branch ###
    - git clone "https://gitlab.com/$TEST_PACKAGE_REPO.git"
    - cd test-package
    - git checkout -b $TEST_BRANCH
    - git branch --set-upstream-to origin/$TEST_BRANCH || true
    - git config user.email "$GITLAB_USER_EMAIL"
    - git config user.name "[auto] $GITLAB_USER_NAME"
    - git rebase $TEST_PACKAGE_DEFAULT_BRANCH

    - |
      echo -e "include:\n  - remote: 'https://gitlab.com/myprojects/ci-cd/main-pipeline/-/raw/$CI_COMMIT_SHA/$PRODUCTION_PIPELINE_FILE'\n    inputs:\n      included_content_ref: $CI_COMMIT_SHA" > .gitlab-ci.yml

    ### Stage, commit and push ###
    - git add .gitlab-ci.yml
    - git commit -m "$MR_TITLE"
    - git push --force origin $TEST_BRANCH

    ### Check whether Testing MR already exists ###
    - set +o pipefail  # Don't fail pipeline on grep fail
    - TEST_MR_URL=$(glab mr list -R $TEST_PACKAGE_REPO | grep "\[AUTO\] .*\/merge_requests\/$CI_MERGE_REQUEST_IID\>" | head -n 1 | cut -f 2 | sed 's,^,https://gitlab.com/,;s,!,/-/merge_requests/,')
    - |
      if [ -z TEST_MR_URL ]; then  ### If not, ...
        ### Create Testing MR ###
        glab mr create --yes --assignee $GITLAB_USER_LOGIN --title "$MR_TITLE" --description "$MR_DESCRIPTION" --head $TEST_PACKAGE_REPO --source-branch $TEST_BRANCH --repo $TEST_PACKAGE_REPO > glab_mr_create_output

        ### Assemble MR note content ###
        TEST_MR_URL=$(grep -o 'https:\/\/gitlab.com.*\/merge_requests/[0-9]\+$' glab_mr_create_output)
        MR_COMMENT="Created testing merge request $TEST_MR_URL"
      else
        MR_COMMENT="Updated testing merge request $TEST_MR_URL"
      fi

    ### Inform user of Testing MR creation/update ###
    - glab mr note -m "$MR_COMMENT" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"

close_test_mr:
  when: manual
  environment:
    name: pipeline-test-$CI_MERGE_REQUEST_IID
    action: stop
  script:
    - set +o pipefail  # Don't fail pipeline on grep fail
    ### Check whether Testing MR exists ###
    - TEST_MR_IID=$(glab mr list -R $TEST_PACKAGE_REPO | grep "\[AUTO\] .*\/merge_requests\/$CI_MERGE_REQUEST_IID\>" | head -n 1 mr_list | cut -f 1 | sed 's/^.//')
    - |
      if [ $TEST_MR_IID ]; then  # If yes, ...
        ### Clone Testing Project, switch to Testing MR branch ###
        git clone "https://gitlab.com/$TEST_PACKAGE_REPO.git"
        cd test-package
        git checkout -b $TEST_BRANCH
        git config user.email "$GITLAB_USER_EMAIL"
        git config user.name "[auto] $GITLAB_USER_NAME"
        git pull origin $TEST_BRANCH

        ### Close Testing MR by deleting its remote branch ###
        git push --force origin :$TEST_BRANCH

        ### Inform user of Testing MR closing ###
        TEST_MR_URL="https://gitlab.com/$TEST_PACKAGE_REPO/-/merge_requests/$TEST_MR_IID"
        glab mr note -m "Closed testing merge request $TEST_MR_URL" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
      fi
